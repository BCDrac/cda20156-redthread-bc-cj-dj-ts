package fr.afpa.redthreadBCCJDJTS.security;

public interface AuthorityConstant {

	public final static String ROLE_USER = "ROLE_USER";
    public final static String ROLE_CONTRIBUTOR = "ROLE_CONTRIBUTOR";
    public final static String ROLE_SUPERCONTRIBUTOR = "ROLE_SUPERCONTRIBUTOR";
    public final static String ROLE_MODERATOR = "ROLE_MODERATOR";
    public final static String ROLE_ADMIN = "ROLE_ADMIN";
    

}
