package fr.afpa.redthreadBCCJDJTS.business;

import java.util.Optional; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.LoginDAO;
import fr.afpa.redthreadBCCJDJTS.entity.DAO.UserDAO;
import fr.afpa.redthreadBCCJDJTS.repository.ILoginRepository;
import fr.afpa.redthreadBCCJDJTS.repository.IUserRepository;

@Service
public class JwtUserLoginDetailService implements UserDetailsService {

    @Autowired
    private IUserRepository userRepository;
    
    @Autowired
    private ILoginRepository loginRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    	LoginDAO login = loginRepository.findByEmail(email);
        Optional<UserDAO> user = userRepository.findById(login.getUser().getId());
        if(!user.isPresent()) throw new UsernameNotFoundException("Utilisateur non trouvé avec l'email : " + email);
        return new User(login.getEmail(), login.getPassword(), user.get().getAuthorities());
    }
}