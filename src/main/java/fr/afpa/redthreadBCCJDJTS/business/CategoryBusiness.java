package fr.afpa.redthreadBCCJDJTS.business;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.CategoryDAO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.CategoryDTO;
import fr.afpa.redthreadBCCJDJTS.modelMapper.CategoryServiceMapper;
import fr.afpa.redthreadBCCJDJTS.repository.ICategoryRepository;

@Service
public class CategoryBusiness {
	
	@Autowired
	CategoryServiceMapper categoryServiceMapper;

	@Autowired 
	ICategoryRepository iCategoryRepository;

	
	public Optional<CategoryDTO> findByName(String name) {
		if (this.iCategoryRepository.findByName(name) != null) {
			return Optional.ofNullable(this.categoryServiceMapper.categoryDaoToCategoryDto(this.iCategoryRepository.findByName(name)));
		}
		return Optional.ofNullable(null);
	}

	public CategoryDTO findById(Long id) {
		return this.categoryServiceMapper.categoryDaoToCategoryDto(this.iCategoryRepository.findById(id).orElse(null));
	}
	
	public CategoryDAO save(CategoryDTO categoryDto) {
		return this.iCategoryRepository.save(this.categoryServiceMapper.categoryDtoToCategoryDao(categoryDto));
	}
	
	public CategoryDAO saveAndFlush(CategoryDTO categoryDto) {
		return this.iCategoryRepository.saveAndFlush(this.categoryServiceMapper.categoryDtoToCategoryDao(categoryDto));
	}

}