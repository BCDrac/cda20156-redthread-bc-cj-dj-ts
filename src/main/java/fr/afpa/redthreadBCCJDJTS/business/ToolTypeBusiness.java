package fr.afpa.redthreadBCCJDJTS.business;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.ToolTypeDAO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.ToolTypeDTO;
import fr.afpa.redthreadBCCJDJTS.modelMapper.ToolTypeServiceMapper;
import fr.afpa.redthreadBCCJDJTS.repository.IToolTypeRepository;

@Service
public class ToolTypeBusiness {
	
	@Autowired
	ToolTypeServiceMapper toolTypeServiceMapper;
	
	@Autowired
	IToolTypeRepository iToolTypeRepository;


	public ToolTypeDAO save(ToolTypeDTO toolTypeDto) {
		return this.iToolTypeRepository.save(this.toolTypeServiceMapper.toolTypeDtoToToolTypeDao(toolTypeDto));
	}
	
	public ToolTypeDAO saveAndFlush(ToolTypeDTO toolTypeDto) {
		return this.iToolTypeRepository.saveAndFlush(this.toolTypeServiceMapper.toolTypeDtoToToolTypeDao(toolTypeDto));
	}

	public Optional <ToolTypeDTO> findByName(String name) {
		if (this.iToolTypeRepository.findByName(name) != null) {
			return Optional.ofNullable(this.toolTypeServiceMapper.toolTypeDaoToToolTypeDto((iToolTypeRepository.findByName(name))));
		}
		return Optional.ofNullable(null);
	}

}