package fr.afpa.redthreadBCCJDJTS.business;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.redthreadBCCJDJTS.entity.DTO.BrandDTO;
import fr.afpa.redthreadBCCJDJTS.modelMapper.BrandServiceMapper;
import fr.afpa.redthreadBCCJDJTS.repository.IBrandRepository;

@Service
public class BrandBusiness {
	
	@Autowired
	BrandServiceMapper brandServiceMapper;
	@Autowired
	IBrandRepository iBrandRepository;
	
	public Optional <BrandDTO> findByName(String name) {
		if (this.iBrandRepository.findByName(name) != null) {
			return Optional.ofNullable(this.brandServiceMapper.brandDaoToBrandDto(this.iBrandRepository.findByName(name)));			
		}
		return Optional.ofNullable(null);
	}

}