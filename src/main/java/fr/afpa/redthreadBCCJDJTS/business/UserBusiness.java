package fr.afpa.redthreadBCCJDJTS.business;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.AuthorityDAO;
import fr.afpa.redthreadBCCJDJTS.entity.DAO.LoginDAO;
import fr.afpa.redthreadBCCJDJTS.entity.DAO.UserDAO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.UserDTO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.UserSecurity;
import fr.afpa.redthreadBCCJDJTS.modelMapper.UserServiceMapper;
import fr.afpa.redthreadBCCJDJTS.repository.ILoginRepository;
import fr.afpa.redthreadBCCJDJTS.repository.IUserRepository;
import fr.afpa.redthreadBCCJDJTS.security.AuthorityConstant;

@Service
public class UserBusiness {
	
	@Autowired
	private UserServiceMapper userServiceMapper;
	
	@Autowired
	private IUserRepository iUserRepository;
	
	@Autowired
	private ILoginRepository iLoginRepository;
	
	
	@Autowired
    private PasswordEncoder passwordEncoder;
	
	
	public Optional<UserDTO> getByUserName(String userName){
		if (iUserRepository.findByUsername(userName) != null) {
			return Optional.ofNullable(userServiceMapper.userDaoToUserDto(iUserRepository.findByUsername(userName)));
		}
		return Optional.ofNullable(null);
	}


    public Long save(UserSecurity userSecurity) {
        UserDAO userDAO = new UserDAO();
        LoginDAO login = new LoginDAO();
        login.setEmail(userSecurity.getEmail());
        login.setPassword(passwordEncoder.encode(userSecurity.getPassword())); // Encode / encrypte le password avec PasswordEncoder
        userDAO.setUsername(userSecurity.getUsername());
        userDAO.setLogin(login);
        login.setUser(userDAO);
        if(userSecurity.getAuthorities().size() == 0) {
            AuthorityDAO authority = new AuthorityDAO(AuthorityConstant.ROLE_USER);
            Set<AuthorityDAO> authorities = new HashSet<>();
            authorities.add(authority);
            userDAO.setAuthorities(authorities);
        } else {
            userDAO.setAuthorities(
                    userSecurity.getAuthorities()
                            .stream()
                            .map(AuthorityDAO::new)
                            .collect(Collectors.toSet())
            );
        }
        return this.iUserRepository.save(userDAO).getId();
    }


    public Optional<UserDTO> getByEmail(String email) {
    	System.out.println(Optional.ofNullable(userServiceMapper.userDaoToUserDto(iLoginRepository.findByEmail(email).getUser())));
    	if (iLoginRepository.findByEmail(email) != null ) {
    		return Optional.ofNullable(userServiceMapper.userDaoToUserDto(iLoginRepository.findByEmail(email).getUser()));
    	}
    	return Optional.ofNullable(null);
    }
}
