package fr.afpa.redthreadBCCJDJTS.business;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.redthreadBCCJDJTS.entity.DTO.PhotoDTO;
import fr.afpa.redthreadBCCJDJTS.modelMapper.PhotoServiceMapper;
import fr.afpa.redthreadBCCJDJTS.repository.IPhotoRepository;

import java.io.*;
import java.nio.file.*;
 
import org.springframework.web.multipart.MultipartFile;
@Service
public class PhotoStorageBusiness {

  @Autowired
  private IPhotoRepository iPhotoRepository ;
  @Autowired
  private PhotoServiceMapper mapper;
  
  private final Path root = Paths.get("user-photos/2");

 
  public void saveFile(String uploadDir, String fileName, MultipartFile multipartFile) throws IOException {
		  Path uploadPath = Paths.get(uploadDir);
		  fileName="1.jpg";
		  if (!Files.exists(uploadPath)) {
		  Files.createDirectories(uploadPath);
		  }
		  
		  try (InputStream inputStream = multipartFile.getInputStream()) {
		  Path filePath = uploadPath.resolve(fileName);
		  PhotoDTO photoDto = new PhotoDTO();
		  photoDto.setLink(filePath.toString());
		  iPhotoRepository.saveAndFlush(this.mapper.photoDtoToPhotoDao(photoDto));
		  System.out.println(filePath+"+++++++++++++++++++++");
		  Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
		  } 
		  catch (IOException ioe) {
			  throw new IOException("Could not save image file: " + fileName, ioe);
		  }
  }
  
  /* public void init() {
  try {
    Files.createDirectory(root);
  } catch (IOException e) {
    throw new RuntimeException("Could not initialize folder for upload!");
  }
}
*/
  public void save(MultipartFile file) {
    try {
      Files.copy(file.getInputStream(), this.root.resolve(file.getOriginalFilename()));
    } catch (Exception e) {
      throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
    }
  }

  public Resource load(String filename) {
    try {
      Path file = root.resolve(filename);
      Resource resource = new UrlResource(file.toUri());

      if (resource.exists() || resource.isReadable()) {
        return resource;
      } else {
        throw new RuntimeException("Could not read the file!");
      }
    } catch (MalformedURLException e) {
      throw new RuntimeException("Error: " + e.getMessage());
    }
  }

  public void deleteAll() {
    FileSystemUtils.deleteRecursively(root.toFile());
  }

  public Stream<Path> loadAll() {
    try {
      return Files.walk(this.root, 1).filter(path -> !path.equals(this.root)).map(this.root::relativize);
    } catch (IOException e) {
      throw new RuntimeException("Could not load the files!");
    }
  }

}
