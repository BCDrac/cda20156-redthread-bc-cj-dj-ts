package fr.afpa.redthreadBCCJDJTS.business;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.StoreDAO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.StoreDTO;
import fr.afpa.redthreadBCCJDJTS.modelMapper.StoreServiceMapper;
import fr.afpa.redthreadBCCJDJTS.repository.IStoreRepository;

@Service
public class StoreBusiness {
	
	@Autowired
	private StoreServiceMapper storeServiceMapper;
	
	@Autowired
	private IStoreRepository iStoreRepository;
	
	
	public StoreDTO findById(Long id) {
		return this.storeServiceMapper.storeDaoToStoreDto(this.iStoreRepository.findById(id).orElse(null));
	}
	
	public StoreDAO save(StoreDTO storeDto) {
		return this.iStoreRepository.save(this.storeServiceMapper.storeDtoToStoreDao(storeDto));
	}
	
	public StoreDAO saveAndFlush(StoreDTO storeDto) {
		return this.iStoreRepository.saveAndFlush(this.storeServiceMapper.storeDtoToStoreDao(storeDto));
	}
	
	public Optional<StoreDTO> findByAddress(StoreDTO store) {
		if (this.iStoreRepository.findByStreetNumberAndStreetNameAndCityAndPostalCodeAndCountry(
				store.getStreetNumber(), store.getStreetName(),
				store.getCity(), store.getPostalCode(), store.getCountry()) != null) {

			return Optional.ofNullable(this.storeServiceMapper.storeDaoToStoreDto(
				this.iStoreRepository.findByStreetNumberAndStreetNameAndCityAndPostalCodeAndCountry(
					store.getStreetNumber(), store.getStreetName(), store.getCity(),
					store.getPostalCode(), store.getCountry()
					)));
		}
		return Optional.ofNullable(null);
	}


}