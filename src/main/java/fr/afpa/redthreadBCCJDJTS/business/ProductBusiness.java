package fr.afpa.redthreadBCCJDJTS.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.ProductDAO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.ProductDTO;
import fr.afpa.redthreadBCCJDJTS.modelMapper.ProductServiceMapper;
import fr.afpa.redthreadBCCJDJTS.repository.IProductRepository;

@Service
public class ProductBusiness {
	
	@Autowired
	private ProductServiceMapper productServiceMapper;
	
	@Autowired
	private IProductRepository iProductRepository;


	public ProductDTO findById(Long id){
        return this.productServiceMapper.productDaoToProductDto(this.iProductRepository.findById(id).orElse(null));
	}
	
	public ProductDAO findByIdDao(Long id) {
		return this.iProductRepository.findById(id).orElse(null);
	}
	
	public Long saveAndFlush(ProductDTO productDto) {
		return this.iProductRepository.saveAndFlush(this.productServiceMapper.productDtoToProductDao(productDto)).getId();
	}

}