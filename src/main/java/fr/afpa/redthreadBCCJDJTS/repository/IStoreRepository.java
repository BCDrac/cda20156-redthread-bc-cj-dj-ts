package fr.afpa.redthreadBCCJDJTS.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.StoreDAO;

public interface IStoreRepository extends JpaRepository<StoreDAO, Long> {
	
	StoreDAO findByStreetNumberAndStreetNameAndCityAndPostalCodeAndCountry(String streetNumber, String streetName, String City, String postalCode, String country);

}