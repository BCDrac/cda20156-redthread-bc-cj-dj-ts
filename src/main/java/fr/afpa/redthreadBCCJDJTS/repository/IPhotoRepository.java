package fr.afpa.redthreadBCCJDJTS.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.PhotoDAO;

public interface IPhotoRepository extends JpaRepository<PhotoDAO, Long> {

}