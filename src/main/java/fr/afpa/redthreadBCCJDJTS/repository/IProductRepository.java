package fr.afpa.redthreadBCCJDJTS.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.ProductDAO;

public interface IProductRepository extends JpaRepository<ProductDAO, Long> {

}