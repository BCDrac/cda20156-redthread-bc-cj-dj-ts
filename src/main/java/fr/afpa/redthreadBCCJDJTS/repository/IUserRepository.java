package fr.afpa.redthreadBCCJDJTS.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.LoginDAO;
import fr.afpa.redthreadBCCJDJTS.entity.DAO.UserDAO;

public interface IUserRepository extends JpaRepository<UserDAO, Long> {
	
	UserDAO findByUsername(String username);
	
	UserDAO findByLogin(LoginDAO login);

}