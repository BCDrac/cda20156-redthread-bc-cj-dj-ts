package fr.afpa.redthreadBCCJDJTS.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.LoginDAO;

public interface ILoginRepository extends JpaRepository<LoginDAO, Long> {
	
	LoginDAO findByEmail(String email);

}