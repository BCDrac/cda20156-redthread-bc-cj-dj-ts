package fr.afpa.redthreadBCCJDJTS.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.AuthorityDAO;

public interface IAuthorityRepository extends JpaRepository<AuthorityDAO, String> {

}