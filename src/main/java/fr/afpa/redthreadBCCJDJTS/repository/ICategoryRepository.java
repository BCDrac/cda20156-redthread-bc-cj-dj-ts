package fr.afpa.redthreadBCCJDJTS.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.CategoryDAO;

public interface ICategoryRepository extends JpaRepository<CategoryDAO, Long> {
	
	CategoryDAO findByName(String name);
}