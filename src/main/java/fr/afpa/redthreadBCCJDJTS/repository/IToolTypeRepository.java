package fr.afpa.redthreadBCCJDJTS.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.ToolTypeDAO;

public interface IToolTypeRepository extends JpaRepository<ToolTypeDAO, Long> {
	
	ToolTypeDAO findByName(String name);

}