package fr.afpa.redthreadBCCJDJTS.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.BrandDAO;

public interface IBrandRepository extends JpaRepository<BrandDAO, Long> {

	BrandDAO findByName(String name);
}