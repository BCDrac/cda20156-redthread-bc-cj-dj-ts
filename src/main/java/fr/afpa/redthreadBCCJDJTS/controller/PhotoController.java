package fr.afpa.redthreadBCCJDJTS.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.util.http.parser.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import antlr.Token;
import fr.afpa.redthreadBCCJDJTS.business.JwtUserLoginDetailService;
import fr.afpa.redthreadBCCJDJTS.business.PhotoStorageBusiness;
import fr.afpa.redthreadBCCJDJTS.business.UserBusiness;
import fr.afpa.redthreadBCCJDJTS.entity.DAO.UserDAO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.PhotoDTO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.UserDTO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.UserSecurity;
import fr.afpa.redthreadBCCJDJTS.security.JwtFilter;
import fr.afpa.redthreadBCCJDJTS.security.JwtUtil;
import io.jsonwebtoken.Jwts;


	@Controller
	
	public class PhotoController { 

	  @Autowired
	  PhotoStorageBusiness photoStorageBusiness ;
	  @Autowired
	   private JwtUtil jwtUtil;
	  @Autowired
	   private JwtFilter f;
	  @Autowired
		private UserBusiness userBusiness;
	  
	  @Autowired
	    private JwtUserLoginDetailService jwtUserDetailService;
	  /**
	   * 
	   * @author sofiane
	   * la méthode d'uplaod d'une photo
	   * @param file la photo uplaodé
	   * @param request 
	   * @return la réponse de l'uplaod
	   */
	  @Secured({"ROLE_USER"}) 
	  @PostMapping("/upload")
	  public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
	    String message = "";
	    String token = f.resolveToken(request);
	    String username = jwtUtil.getSubject(token);
	    Optional<UserDTO> user = userBusiness.getByEmail(username);
	    System.out.println(user.get().getId()+"+++++++++++++++++++++++++++++++++++++++++");
	    try {
	    	
	      String fileName = StringUtils.cleanPath(file.getOriginalFilename());
	      String uploadDir = "user-photos/user-" + user.get().getId(); //ceci est le dossier d'enregistrement de la photo 
	      photoStorageBusiness.saveFile(uploadDir, fileName, file);
	      message = "Uploaded the file successfully: " + file.getOriginalFilename();
	      return ResponseEntity.status(HttpStatus.OK).body(message);
	    } catch (Exception e) {
	      message = "Could not upload the file: " + file.getOriginalFilename() + "!";
	      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
	    }
	  }
	  
	  /**
	   * 
	   * @author sofiane
	   * la méthode de récupération des photos
	   * @return la liste des photos
	   */
	  
	  @GetMapping("/files")
	  public ResponseEntity<List<PhotoDTO>> getListFiles() {
	    List<PhotoDTO> fileInfos = photoStorageBusiness.loadAll().map(path -> {
	      String filename = path.getFileName().toString();
	      String url = MvcUriComponentsBuilder
	          .fromMethodName(PhotoController.class, "getFile", path.getFileName().toString()).build().toString();

	      return new PhotoDTO(1L, url, true);
	    }).collect(Collectors.toList());

	    return ResponseEntity.status(HttpStatus.OK).body(fileInfos);
	  }
	  
	  /**
	   * 
	   * @author sofiane
	   * la méthode de récupération d'une photo selon son nom
	   * @return la la photo trouvée
	   */
	  @GetMapping("/files/{filename:.+}")
	  public ResponseEntity<Resource> getFile(@PathVariable String filename) {
	    Resource file = photoStorageBusiness.load(filename);
	    return ResponseEntity.ok()
	        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
	  }

/*	@Override
	public void run(String... args) throws Exception {
		 	//storageService.deleteAll();
		    //storageService.init();
		
	}*/
	}
