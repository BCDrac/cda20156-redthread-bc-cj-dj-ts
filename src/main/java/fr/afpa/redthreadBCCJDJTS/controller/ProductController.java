package fr.afpa.redthreadBCCJDJTS.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.hibernate.mapping.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.redthreadBCCJDJTS.business.BrandBusiness;
import fr.afpa.redthreadBCCJDJTS.business.CategoryBusiness;
import fr.afpa.redthreadBCCJDJTS.business.ProductBusiness;
import fr.afpa.redthreadBCCJDJTS.business.StoreBusiness;
import fr.afpa.redthreadBCCJDJTS.business.ToolTypeBusiness;
import fr.afpa.redthreadBCCJDJTS.business.UserBusiness;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.BrandDTO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.CategoryDTO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.ProductDTO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.StoreDTO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.ToolTypeDTO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.UserDTO;

@RestController
public class ProductController {

	@Autowired
	private BrandBusiness brandBusiness;
	@Autowired
	private CategoryBusiness categoryBusiness;
	@Autowired
	private ProductBusiness productBusiness;
	@Autowired
	private StoreBusiness storeBusiness;
	@Autowired
	private ToolTypeBusiness toolTypeBusiness;
	@Autowired
	private UserBusiness userBusiness;

	
	
	@GetMapping("/product") 
	public BodyBuilder getProduct(Long id){
		ProductDTO productDTO = this.productBusiness.findById(id);
		return (BodyBuilder) ResponseEntity.ok(productDTO);
	}


	@PostMapping("/product")
	public ResponseEntity<Object> postProduct(@RequestBody ProductDTO product) {

		if (product != null) {

			StoreDTO store = product.getStores() != null ? product.getStores().get(0) : null;
			List <CategoryDTO> categories = product.getCategories() != null ? product.getCategories() : null;
			String username = product.getUser().getUsername() != null ? product.getUser().getUsername() : null;
			
			BrandDTO brand = null;
			ToolTypeDTO toolType = null;
			
			if (product.getBrand() != null) {
				brand = this.brandBusiness.findByName(product.getBrand().getName()).isPresent()
					? this.brandBusiness.findByName(product.getBrand().getName()).get()
					: null;
			}
			
			if (product.getToolType() != null) {
				toolType = this.toolTypeBusiness.findByName(product.getToolType().getName()).isPresent()
					? this.toolTypeBusiness.findByName(product.getToolType().getName()).get()
					: null;
			}

			if (isProductComplete(product) && username != null && !username.trim().isEmpty() && toolType != null) {

				UserDTO user = this.userBusiness.getByUserName(username).get();

				if (user != null && categories != null && brand != null && toolType != null) {

					this.assignProductToUser(product, user);
					this.assignProductToToolType(product, toolType);
					this.assignProductToBrand(product, brand);

					if (this.areCategoriesPresentInDatabase(product, categories)) {
						
						this.assignProductAndCategories(product, categories);

						for (CategoryDTO category : categories) {
							this.categoryBusiness.saveAndFlush(category);
							
							for (CategoryDTO subcategory : category.getSubcategories()) {
								this.categoryBusiness.saveAndFlush(subcategory);
							}
						}

						if (store != null) {

							if (this.isStoreComplete(store)) {

								if (this.storeBusiness.findByAddress(store).isPresent()) {
									StoreDTO databasetoUpdate = this.storeBusiness.findByAddress(store).get();
									this.assignProductAndStore(product, databasetoUpdate);
									//this.storeBusiness.saveAndFlush(databasetoUpdate);

								} else {
									this.assignProductAndStore(product, store);
									//this.storeBusiness.saveAndFlush(store);
								}
							} else {
								return ResponseEntity.unprocessableEntity().build();
							}
						}
						Long returnedId = this.productBusiness.saveAndFlush(product);
						return ResponseEntity.created(URI.create("/product/" + returnedId)).build();
					}
				}
			}
		}
		return ResponseEntity.unprocessableEntity().build();
	}

	/*
	@PutMapping("/product/{id}")
	public BodyBuilder patchProduct(Long id, @RequestBody ProductDAO product, List<StoreDTO> stores) {
		
		ProductDAO productToUpdate = this.productBusiness.findByIdDao(id);
		
		if (productToUpdate != null) {
			productToUpdate = product;
			
			if (stores != null) {
				for (StoreDTO store : stores) {
					StoreDTO storeDao = this.storeBusiness.findById(store.getId());
					if (storeDao == null) {
						productToUpdate.getStores().add(store);
					} else {
						storeDao = store;
						this.storeBusiness.save(storeDao);
					}
				}
			}
			Long returnedId = this.productBusiness.saveAndFlush(productToUpdate);
			return (BodyBuilder) ResponseEntity.created(URI.create("/product/" + returnedId)).build();
		}
		System.out.println("Error");
		return (BodyBuilder) ResponseEntity.unprocessableEntity();	
	}*/
	
	
	/**
	 * Associe le type d'outil au produit
	 * 
	 * @param product : le produit à associer
	 * @param toolType : le type d'outil à associer
	 * 
	 * @author Cécile
	 */
	private void assignProductToToolType(ProductDTO product, ToolTypeDTO toolType) {
		if (toolType != null) {
			product.setToolType(toolType);

			if (toolType.getProducts() == null) {
				List<ProductDTO> products = new ArrayList<ProductDTO>();
				products.add(product);
				toolType.setProducts(products);
			} else {
				toolType.getProducts().add(product);
			}
		}
	}

	/**
	 * Associe le produit à son auteur
	 * 
	 * @param product : le produit à associer
	 * @param user : l'utilisateur à associer
	 * 
	 * @author Cécile
	 */
	private void assignProductToUser(ProductDTO product, UserDTO user) {
		if (user.getCreatedProducts() == null) {
			List<ProductDTO> products = new ArrayList<ProductDTO>();
			products.add(product);
			user.setCreatedProducts(products);
			product.setUser(user);
		} else {
			user.getCreatedProducts().add(product);
			product.setUser(user);
		}
	}
	
	/**
	 * Associe produit et magasin
	 * @param product : le produit à associer
	 * @param store : lee magasin à associer
	 * 
	 * @author Cécile
	 */
	private void assignProductAndStore(ProductDTO product, StoreDTO store) {

		if (product.getStores() == null) {
			List<StoreDTO> stores = new ArrayList<StoreDTO>();
			product.setStores(stores);
			product.getStores().add(store);

		} else {
			product.getStores().add(store);
		}

		if (store.getProducts() == null) {
			HashSet<ProductDTO> products = new HashSet<ProductDTO>();
			store.setProducts(products);
			store.getProducts().add(product);
			
		} else {
			store.getProducts().add(product);
		}
	}
	
	
	/**
	 * Assoccie le produit aux catégories
	 * 
	 * @param product : le produit à associer
	 * @param categories : les catégories à associer
	 * 
	 * @author Cécile
	 */
	private boolean areCategoriesPresentInDatabase(ProductDTO product, List<CategoryDTO> categories) {
		
		boolean isAssigned = true;

		for (CategoryDTO category : categories) {

			if (category != null && this.categoryBusiness.findByName(category.getName()).isPresent()) {

				if (category.getSubcategories() != null) {

					for (CategoryDTO subcategory : category.getSubcategories()) {

						if (this.categoryBusiness.findByName(subcategory.getName()).isEmpty()) { 
							isAssigned = false;
							break;
						}
					}
				} else { isAssigned = false; }
			} else { isAssigned = false; }
		}
		return isAssigned;
	}
	
	
	private void assignProductAndCategories(ProductDTO product, List<CategoryDTO> categories) {
		
		int i = 0;
		int j = 0;
		int k = 0;
		
		for (CategoryDTO category : categories) {

			category = this.categoryBusiness.findByName(category.getName()).get();

			if (category.getProducts() == null) {
				category.setProducts(new HashSet<ProductDTO>());
				category.getProducts().add(product);
				categories.set(i, category);
				i++;
			} else {
				category.getProducts().add(product);
				categories.set(i, category);
				i++;
			}

			for (CategoryDTO subcategory : category.getSubcategories()) {

				subcategory = this.categoryBusiness.findByName(subcategory.getName()).get();

				if (subcategory.getProducts() == null) {
					subcategory.setProducts(new HashSet<ProductDTO>());
					subcategory.getProducts().add(product);
					categories.get(j).getSubcategories().set(k, subcategory);
					j++; k++;

				} else {
					subcategory.setParent(category);
					subcategory.getProducts().add(product);
					categories.get(j).getSubcategories().set(k, subcategory);
					j++; k++;
				}
			}
			product.setCategories(categories);
		}
	}
	
	
	/**
	 * Associe le produit à la marque
	 * 
	 * @param product : le produit à associer
	 * @param brand : la marque à associer
	 */
	private void assignProductToBrand(ProductDTO product, BrandDTO brand) {

		if (brand.getProducts() == null) {
			List<ProductDTO> products = new ArrayList<ProductDTO>();
			products.add(product);
			brand.setProducts(products);
			product.setBrand(brand);

		} else {
			brand.getProducts().add(product);
			product.setBrand(brand);
		}
	}
	
	
	/**
	 * Vérifie que le produit en création est complet
	 * 
	 * @param product : le produit à vérifier
	 * @return booléenne : retourne true si le produit est bien complété
	 */
	private boolean isProductComplete(ProductDTO product) {
		return this.isProductNamePresent(product.getName()) 
			&& this.isProductBrandPresent(product.getBrand())
			&& this.isProductDescriptionPresent(product.getDescription())
			&& this.isProductCategoryPresent(product.getCategories())
			&& this.isProductSubcategoryPresent(product.getCategories())
			&& this.isToolTypePresent(product.getToolType());
	}
	
	private boolean isProductNamePresent(String name) {
		return (name != null && !name.trim().isEmpty());
	}
	
	private boolean isProductBrandPresent(BrandDTO brand) {
		return (brand != null && !brand.getName().trim().isEmpty());
	}
	
	private boolean isProductDescriptionPresent(String description) {
		return (description != null && !description.trim().isEmpty() && description.length() <= 400);
	}
	
	private boolean isProductCategoryPresent(List<CategoryDTO> categories) {
		return (categories != null && !categories.isEmpty() && !categories.iterator().next().getName().isEmpty());
	}

	private boolean isProductSubcategoryPresent(List<CategoryDTO> categories) {
		return (categories != null && !categories.isEmpty()
				&& !categories.iterator().next().getSubcategories().iterator().next().getName().isEmpty());
	}
	
	private boolean isToolTypePresent(ToolTypeDTO toolType) {
		return (toolType != null && !toolType.getName().trim().isEmpty());
	}
	
	
	private boolean isStoreComplete(StoreDTO store) {

		return this.isStoreNamePresent(store.getName()) 
			&& this.isStoreStreetNumberPresent(store.getStreetNumber())
			&& this.isStoreStreetNamePresent(store.getStreetName())
			&& this.isStoreCityPresent(store.getCity())
			&& this.isStorePostalCodePresent(store.getPostalCode())
			&& this.isStorePricePresent(store.getPrice())
			&& this.isCountryPresent(store.getCountry())
		;
	}
	
	private boolean isStoreNamePresent(String name) {
		return (name != null && !name.trim().isEmpty());
	}
	
	private boolean isStoreStreetNumberPresent(String streetNumber) {
		return (streetNumber != null && !streetNumber.trim().isEmpty());
	}
	
	private boolean isStoreStreetNamePresent(String streetName) {
		return (streetName != null && !streetName.trim().isEmpty());
	}
	
	private boolean isStoreCityPresent(String city) {
		return (city != null && !city.trim().isEmpty());
	}
	
	private boolean isStorePostalCodePresent(String postalCode) {
		return (postalCode != null && !postalCode.trim().isEmpty());
	}
	
	private boolean isStorePricePresent(Double price) {
		return (price != null && price != 0);
	}
	
	private boolean isCountryPresent(String country) {
		return (country != null && !country.trim().isEmpty());
	}
}