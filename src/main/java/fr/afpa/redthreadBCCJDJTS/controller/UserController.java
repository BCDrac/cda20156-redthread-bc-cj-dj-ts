package fr.afpa.redthreadBCCJDJTS.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.redthreadBCCJDJTS.business.JwtUserLoginDetailService;
import fr.afpa.redthreadBCCJDJTS.business.UserBusiness;
import fr.afpa.redthreadBCCJDJTS.controller.exception.EmailAlreadyExistsException;
import fr.afpa.redthreadBCCJDJTS.controller.exception.UsernameAlreadyExistsException;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.AuthenticationResponse;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.UserDTO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.UserSecurity;
import fr.afpa.redthreadBCCJDJTS.security.AuthorityConstant;
import fr.afpa.redthreadBCCJDJTS.security.JwtUtil;

@RestController
public class UserController {
	
	@Autowired
	private UserBusiness userBusiness;
	
    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private JwtUserLoginDetailService userLoginDetailService;

    @Autowired
    private AuthenticationManager authenticationManager;
	
	private Long idSaved;
	
	
	@PostMapping("/register")
    public ResponseEntity<Long> register(@RequestBody UserSecurity user) {
		
		Optional<UserDTO> userCheckForEmail = userBusiness.getByEmail(user.getEmail());
        Optional<UserDTO> userCheckForUsername = userBusiness.getByUserName(user.getUsername());
        
        if(isEmailUnique(userCheckForEmail) && isUserNameUnique(userCheckForUsername)) {
        	idSaved = userBusiness.save(user);
        } else if (!isEmailUnique(userCheckForEmail)) {
        	//React email déjà existant
        } else if (!isUserNameUnique(userCheckForUsername)) {
        	//React pseudo déjà existant
        }
        return ResponseEntity.ok(idSaved);
    }
	
	/**
	 * @author jerome
	 * 
	 * Check si le mail du registration existe déjà en DB
	 * @param userCheckMail
	 * @return
	 */
	public boolean isEmailUnique(Optional<UserDTO> userCheckMail) {
		boolean isEmailUnique = true;
		if(userCheckMail.isPresent()) {
			isEmailUnique = false;
        	throw new EmailAlreadyExistsException();
        }
		return isEmailUnique;
	}
	
	/**
	 * @author jerome
	 * 
	 * check si le username du registration existe déjà en DB
	 * @param userCheckUsername
	 * @return
	 */
	public boolean isUserNameUnique(Optional<UserDTO> userCheckUsername) {
		boolean isUsernameUnique = true;
		if(userCheckUsername.isPresent()) {
			isUsernameUnique = false;
        	throw new UsernameAlreadyExistsException();
        }
		return isUsernameUnique;
	}
	

	@GetMapping("/hello")
    public String hello() {
        return "Hello world";
    }

    @Secured({"ROLE_USER"}) //l'un ou l'autre
    @GetMapping("/hello-user")
    public String helloAdmin() {
        return "Hello user";
    }

    @Secured(AuthorityConstant.ROLE_ADMIN)
    @GetMapping("/hello-admin")
    public String helloUser() {
        return "Hello admin";
    }
    
    
    /**
     * Request REST: {POST /login}
     * Permet de se connecter, si on reçoit des Credentials correctes
     * Si cela est bon, envoi au front un objet avec le JsonWebToken et le username de l'utilisateur
     *
     * @param userSecurity
     * @return AuthenticationResponse
     */
    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponse> authenticate(@RequestBody UserSecurity userSecurity) {
        UsernamePasswordAuthenticationToken tokenSpring = new UsernamePasswordAuthenticationToken(
                userSecurity.getEmail(), userSecurity.getPassword()
        );
        try {
            //L'authentification se fait ici. C'est SpringSecurity qui vérifie les Crédentials reçu, et va pouvoir m'authentifier
            //Dans le cas contraire, renvoi une exception BadCredentialsException
            authenticationManager.authenticate(tokenSpring);
        } catch (BadCredentialsException ex) {
            // -> email ou mot de passe incorrect
        }
        //Je récupére le UserDetails coresspondant à l'email reçu
        UserDetails userDetails = userLoginDetailService.loadUserByUsername(userSecurity.getEmail());
        //Avec UserDetails, je crée mon JsonWebToken
        String jwt = jwtUtil.generateToken(userDetails);
        //Je renvoi au front l'objet AuthenticationResponse contenant le JWT et username de l'utilisateur
        //Toutes requetes sécurisé en back aura donc besoin de ce JWT pour être effectué
        return ResponseEntity.ok(new AuthenticationResponse(jwt, userDetails.getUsername()));
    }
}
