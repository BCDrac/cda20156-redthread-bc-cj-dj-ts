package fr.afpa.redthreadBCCJDJTS.controller.exception;

public class EmailAlreadyExistsException extends RuntimeException {
	
	public EmailAlreadyExistsException() {
        super("Cet email est associé à un autre utilisateur du site");
    }

}
