package fr.afpa.redthreadBCCJDJTS.controller.exception;

public class UsernameAlreadyExistsException extends RuntimeException {

    public UsernameAlreadyExistsException() {
        super("Ce pseudo existe déjà");
    }
}