package fr.afpa.redthreadBCCJDJTS.entity.DAO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "photo")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PhotoDAO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "photo_seq")
	@SequenceGenerator(name = "photo_seq", sequenceName = "photo_seq", allocationSize = 1)
	private Long id;
	
	@Column(nullable = false)
	private String link;

	private boolean active;
	
	@OneToOne(mappedBy = "avatar")
	private UserDAO avatarOwner;
	
	@OneToOne(mappedBy = "banner")
	private UserDAO bannerOwner;
	
	@ManyToOne()
	@JoinColumn(name = "id_comment", referencedColumnName = "id", nullable = true)
	private CommentDAO comment;
	
	@ManyToOne()
	@JoinColumn(name = "id_product", referencedColumnName = "id", nullable = true)
	private ProductDAO product;
}