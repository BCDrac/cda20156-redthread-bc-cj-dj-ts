package fr.afpa.redthreadBCCJDJTS.entity.DAO;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "users")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserDAO {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_seq")
	@SequenceGenerator(name = "users_seq", sequenceName = "users_seq", allocationSize = 1)
	private Long id;
	@Column(unique = true, nullable = false)
	private String username;

	@Column(name = "register_date")
	private Date registerDate;

	private boolean active;

	private boolean canCreate;

	@OneToOne
	@JoinColumn(name = "id_user_avatar", referencedColumnName = "id")
	private PhotoDAO avatar;

	@OneToOne
	@JoinColumn(name = "id_user_banner", referencedColumnName = "id")
	private PhotoDAO banner;

	@OneToOne(mappedBy = "user", fetch = FetchType.LAZY)
	private LoginDAO login;

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	private List<CommentDAO> comments;
	
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	private List<ProductDAO> createdProducts;
	
	@JsonIgnore
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "users_authority", joinColumns = {
			@JoinColumn(name = "id_user", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "authority_name", referencedColumnName = "name") })
	private Set<AuthorityDAO> authorities = new HashSet<>();
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "users_product", joinColumns = {
			@JoinColumn(name = "id_user", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "id_product", referencedColumnName = "id") })
	private Set<ProductDAO> favorites;
	
	
}