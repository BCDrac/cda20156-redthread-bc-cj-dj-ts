package fr.afpa.redthreadBCCJDJTS.entity.DAO;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "store")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class StoreDAO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "store_seq")
	@SequenceGenerator(name = "store_seq", sequenceName = "store_seq", allocationSize = 1)
	private Long id;
	
	private String name;
	private String streetNumber;
	private String streetName;
	private String complement;
	private String city;
	private String postalCode;
	private String country;
	private Double price;
	private String url;
	private String gps;
	
	@ManyToMany(cascade = {CascadeType.MERGE})
	@JoinTable(
			  name = "store_product",
			  joinColumns = @JoinColumn(name = "id_store"), 
			  inverseJoinColumns = @JoinColumn(name = "id_product"))
	private Set<ProductDAO> products;
}