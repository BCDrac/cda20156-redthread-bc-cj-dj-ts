package fr.afpa.redthreadBCCJDJTS.entity.DAO;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(
		name = "comment",
		uniqueConstraints = { @UniqueConstraint(columnNames = { "id_product", "id_user" }) }
		)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CommentDAO {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "comment_seq")
	@SequenceGenerator(name = "comment_seq", sequenceName = "commment_seq", allocationSize = 1)
	private Long id;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String text;

	@Column(name = "creation_date")
	private Date creationDate;
	private Double note;

	private boolean active;

	@OneToMany(mappedBy = "comment")
	private List<PhotoDAO> photos;

	@ManyToOne()
	@JoinColumn(name = "id_product", referencedColumnName = "id")
	private ProductDAO product;

	@ManyToOne()
	@JoinColumn(name = "id_user", referencedColumnName = "id")
	private UserDAO user;
}