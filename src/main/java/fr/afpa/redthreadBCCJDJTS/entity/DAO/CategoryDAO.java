package fr.afpa.redthreadBCCJDJTS.entity.DAO;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "category")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CategoryDAO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "category_seq")
	@SequenceGenerator(name = "category_seq", sequenceName = "category_seq", allocationSize = 1)
	private Long id;
	@Column(unique = true)
	private String name;
	
	@OneToMany(mappedBy = "parent")
	private List<CategoryDAO> subcategories;

	@ManyToOne(cascade = {CascadeType.REFRESH})
	@JoinColumn(name = "id_parent", referencedColumnName = "id", nullable = true)
	private CategoryDAO parent;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
	@JoinTable(name = "product_category", joinColumns = {
			@JoinColumn(name = "id_category", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "id_product", referencedColumnName = "id") })
	private Set<ProductDAO> products;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
	@JoinTable(name = "tooltype_category", joinColumns = {
			@JoinColumn(name = "id_category", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "id_tool_type", referencedColumnName = "id") })
	private Set<ToolTypeDAO> toolTypes;
}