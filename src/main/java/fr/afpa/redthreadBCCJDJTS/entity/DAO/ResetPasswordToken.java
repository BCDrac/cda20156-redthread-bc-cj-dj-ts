//package fr.afpa.entity.DAO;
//
//import javax.persistence.CascadeType;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.OneToOne;
//import javax.persistence.SequenceGenerator;
//
//import lombok.AllArgsConstructor;
//import lombok.EqualsAndHashCode;
//import lombok.Getter;
//import lombok.NoArgsConstructor;
//import lombok.Setter;
//import lombok.ToString;
//
//@Getter
//@Setter
//@AllArgsConstructor
//@NoArgsConstructor
//@ToString
//@EqualsAndHashCode
//@Entity
//public class ResetPasswordToken {
//
//	
//	@Id
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reset_pw_token")
//    @SequenceGenerator(name = "reset_pw_token", sequenceName = "reset_pw_token", allocationSize = 1)
//    private Long id;
//	private String token;
//	
//	@OneToOne(cascade = { CascadeType.ALL })
//	@JoinColumn(name = "id_login", referencedColumnName = "id")
//	private LoginDAO loginDao;
//	
//}