package fr.afpa.redthreadBCCJDJTS.entity.DAO;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "tooltype")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ToolTypeDAO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tooltype_seq")
	@SequenceGenerator(name = "tooltype_seq", sequenceName = "tooltype_seq", allocationSize = 1)
	private Long id;
	
	@Column(nullable= false, unique = true)
	private String name;
	
	@OneToMany(mappedBy = "toolType")
	private List <ProductDAO> products;
	
	@ManyToMany(mappedBy = "toolTypes")
	private Set <CategoryDAO> categories;
}