package fr.afpa.redthreadBCCJDJTS.entity.DAO;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "login")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LoginDAO {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "login_seq")
	@SequenceGenerator(name = "login_seq", sequenceName = "login_seq", allocationSize = 1)
	private Long id;

	@Column(nullable = false)
	private String email;

	@Column(nullable = false)
	@JsonIgnore
	private String password;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_user", referencedColumnName = "id")
	private UserDAO user;
	
//	@OneToOne(fetch = FetchType.EAGER, mappedBy = "id_login")
//	private ResetPasswordToken resetPasswordToken;
}