package fr.afpa.redthreadBCCJDJTS.entity.DAO;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "product")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductDAO {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_seq")
	@SequenceGenerator(name = "product_seq", sequenceName = "product_seq", allocationSize = 1)
	private Long id;

	private String name;
	private String description;

	@Column(name = "average_price")
	private Double averagePrice;
	private Double note;

	@Column(name = "creation_date")
	private Date creationDate;
	private String reference;
	private boolean active;

	@OneToMany(mappedBy = "product")
	private List<CommentDAO> comments;

	@OneToMany(mappedBy = "product")
	private List<PhotoDAO> photos;

	@ManyToOne(cascade = {CascadeType.REFRESH})
	@JoinColumn(name = "id_tooltype", referencedColumnName = "id")
	private ToolTypeDAO toolType;

	@ManyToOne(cascade = {CascadeType.REFRESH})
	@JoinColumn(name = "id_user", referencedColumnName = "id")
	private UserDAO user;
	
	@ManyToOne(cascade = {CascadeType.REFRESH})
	@JoinColumn(name = "id_brand", referencedColumnName = "id")
	private BrandDAO brand;

	@ManyToMany(mappedBy = "products")
	private Set<CategoryDAO> categories;

	@ManyToMany(mappedBy = "products")
	private Set<StoreDAO> stores;

	@ManyToMany(mappedBy = "favorites")
	private Set<UserDAO> usersOwners;
}