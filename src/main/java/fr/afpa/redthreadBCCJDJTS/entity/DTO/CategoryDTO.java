package fr.afpa.redthreadBCCJDJTS.entity.DTO;

import java.util.List;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CategoryDTO {

	private Long id;
	private String name;
	private CategoryDTO parent;
	private List<CategoryDTO> subcategories;
	private Set<ProductDTO> products;
	private Set<ToolTypeDTO> toolTypes;
}