package fr.afpa.redthreadBCCJDJTS.entity.DTO;

import java.util.Date;
import java.util.List;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductDTO {
	
	private Long id;
	private String name;
	private String description;
	private Double averagePrice;
	private Double note;
	private Date creationDate;
	private String reference;
	private boolean active;

	private List<CommentDTO> comments;
	private List<PhotoDTO> photos;
	
	private BrandDTO brand;
	private ToolTypeDTO toolType;
	private UserDTO user;
	
	private List<CategoryDTO> categories;
	private List<StoreDTO> stores;
	private List<UserDTO> usersOwners;

}