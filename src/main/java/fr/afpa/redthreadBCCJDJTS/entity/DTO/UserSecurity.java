package fr.afpa.redthreadBCCJDJTS.entity.DTO;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

//Les attributs doivent correspondre aux champs du form de l'inscription
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserSecurity {
	
	private String username;
	private String email;
	private String password;
	private List<String> authorities = new ArrayList<>();
}
