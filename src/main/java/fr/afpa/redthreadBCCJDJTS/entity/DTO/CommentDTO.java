package fr.afpa.redthreadBCCJDJTS.entity.DTO;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CommentDTO {
	
	private Long id;
	private String text;
	private Date creationDate;
	private Double note;
	private boolean active;
	
	private List<PhotoDTO> photos;
	
	private ProductDTO product;
	private UserDTO user;

}
