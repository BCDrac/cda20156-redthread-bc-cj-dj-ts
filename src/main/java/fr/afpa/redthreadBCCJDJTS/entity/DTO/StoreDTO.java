package fr.afpa.redthreadBCCJDJTS.entity.DTO;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class StoreDTO {
	
	private Long id;
	private String name;
	private String streetNumber;
	private String streetName;
	private String complement;
	private String city;
	private String postalCode;
	private String country;
	private Double price;
	private String url;
	private String gps;
	
	private Set<ProductDTO> products;

}