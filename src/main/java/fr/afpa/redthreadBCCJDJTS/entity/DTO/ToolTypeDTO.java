package fr.afpa.redthreadBCCJDJTS.entity.DTO;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ToolTypeDTO {
	
	private Long id;
	private String name;
	private List<ProductDTO> products;
	//private List<CategoryDTO> categories;

}