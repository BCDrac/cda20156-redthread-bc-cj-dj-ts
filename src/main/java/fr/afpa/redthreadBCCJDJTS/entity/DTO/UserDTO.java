package fr.afpa.redthreadBCCJDJTS.entity.DTO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.LoginDAO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserDTO {

	private Long id;

	private String username;
	private Date registerDate;

	private boolean active;

	private boolean canCreate;
	
	private String avatar;
	private String banner;
	
	private LoginDAO login;
	private ArrayList <String> comments;
	private ArrayList <String> favorites;
	
	private List<ProductDTO> createdProducts;

	private List<String> authorities;
}