package fr.afpa.redthreadBCCJDJTS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;


@SpringBootApplication
public class RedThreadBcCjDjTsApplication {

	public static void main(String[] args) {
		SpringApplication.run(RedThreadBcCjDjTsApplication.class, args);
	}
}