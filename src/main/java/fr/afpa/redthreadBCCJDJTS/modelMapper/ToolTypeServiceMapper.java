package fr.afpa.redthreadBCCJDJTS.modelMapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.ToolTypeDAO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.ToolTypeDTO;

@Service
public class ToolTypeServiceMapper {
	
	@Autowired
	private ModelMapper modelMapper;
	
	public ToolTypeDAO toolTypeDtoToToolTypeDao(ToolTypeDTO toolTypeDto) {
		ToolTypeDAO toolTypeEntity = new ToolTypeDAO();
		modelMapper.map(toolTypeDto, toolTypeEntity);
        return toolTypeEntity;
    }

    public ToolTypeDTO toolTypeDaoToToolTypeDto(ToolTypeDAO productDao) {
    	ToolTypeDTO innerToolType = new ToolTypeDTO();
    	modelMapper.map(productDao, innerToolType);
        return innerToolType;
    }

}