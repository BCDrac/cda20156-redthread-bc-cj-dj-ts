package fr.afpa.redthreadBCCJDJTS.modelMapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.PhotoDAO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.PhotoDTO;

@Service
public class PhotoServiceMapper {
	
	@Autowired
	private ModelMapper modelMapper;
	
	public PhotoDAO photoDtoToPhotoDao(PhotoDTO photoDto) {
		PhotoDAO photoEntity = new PhotoDAO();
		modelMapper.map(photoDto, photoEntity);
        return photoEntity;
    }

    public PhotoDTO photoDaoToPhotoDto(PhotoDAO photoDao) {
    	PhotoDTO innerPhoto = new PhotoDTO();
    	modelMapper.map(photoDao, innerPhoto);
        return innerPhoto;
    }

}
