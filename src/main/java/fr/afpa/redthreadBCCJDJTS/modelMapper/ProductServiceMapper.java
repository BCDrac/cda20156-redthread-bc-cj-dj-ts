package fr.afpa.redthreadBCCJDJTS.modelMapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.ProductDAO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.ProductDTO;

@Service
public class ProductServiceMapper {
	
	@Autowired
	private ModelMapper modelMapper;
	
	public ProductDAO productDtoToProductDao(ProductDTO productDto) {
		ProductDAO productEntity = new ProductDAO();
		modelMapper.map(productDto, productEntity);
        return productEntity;
    }

    public ProductDTO productDaoToProductDto(ProductDAO productDao) {
    	ProductDTO innerProduct = new ProductDTO();
    	modelMapper.map(productDao, innerProduct);
        return innerProduct;
    }

}