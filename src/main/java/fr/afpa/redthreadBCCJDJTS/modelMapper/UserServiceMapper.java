package fr.afpa.redthreadBCCJDJTS.modelMapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.UserDAO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.UserDTO;

@Service
public class UserServiceMapper {
	
	@Autowired
	private ModelMapper modelMapper;
	
	public UserDAO userDtoToUserDao(UserDTO userDto) {
		UserDAO userEntity = new UserDAO();
		modelMapper.map(userDto, userEntity);
        return userEntity;
    }

    public UserDTO userDaoToUserDto(UserDAO userDao) {
    	UserDTO innerUser = new UserDTO();
    	modelMapper.map(userDao, innerUser);
        return innerUser;
    }

}
