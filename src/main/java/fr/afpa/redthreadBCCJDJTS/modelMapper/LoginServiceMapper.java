package fr.afpa.redthreadBCCJDJTS.modelMapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.LoginDAO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.LoginDTO;

@Service
public class LoginServiceMapper {
	
	@Autowired
	private ModelMapper modelMapper;
	
	public LoginDAO loginDtoToLoginDao(LoginDTO loginDto) {
        LoginDAO loginEntity = new LoginDAO();
        modelMapper.map(loginDto, loginEntity);
        return loginEntity;
    }

    public LoginDTO loginDaoToLoginDto(LoginDAO loginDao) {
        LoginDTO innerLogin = new LoginDTO();
        modelMapper.map(loginDao, innerLogin);
        return innerLogin;
    }

}
