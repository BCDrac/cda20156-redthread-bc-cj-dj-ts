package fr.afpa.redthreadBCCJDJTS.modelMapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.CategoryDAO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.CategoryDTO;

@Service
public class CategoryServiceMapper {
	
	@Autowired
	private ModelMapper modelMapper;
	
	public CategoryDAO categoryDtoToCategoryDao(CategoryDTO categoryDto) {
		CategoryDAO categoryEntity = new CategoryDAO();
        modelMapper.map(categoryDto, categoryEntity);
        return categoryEntity;
    }

    public CategoryDTO categoryDaoToCategoryDto(CategoryDAO categoryDao) {
    	CategoryDTO innerCategory = new CategoryDTO();
    	modelMapper.map(categoryDao, innerCategory);
        return innerCategory;
    }

}
