package fr.afpa.redthreadBCCJDJTS.modelMapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.BrandDAO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.BrandDTO;

@Service
public class BrandServiceMapper {

	@Autowired
	private ModelMapper modelMapper;
	
	public BrandDAO brandDtoToBrandDao(BrandDTO brandDto) {
		BrandDAO brandEntity = new BrandDAO();
        modelMapper.map(brandDto, brandEntity);
        return brandEntity;
    }

    public BrandDTO brandDaoToBrandDto(BrandDAO brandDao) {
    	BrandDTO innerBrand = new BrandDTO();
    	modelMapper.map(brandDao, innerBrand);
        return innerBrand;
    }
	
}