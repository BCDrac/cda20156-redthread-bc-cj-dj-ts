package fr.afpa.redthreadBCCJDJTS.modelMapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.CommentDAO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.CommentDTO;

@Service
public class CommentServiceMapper {
	
	@Autowired
	private ModelMapper modelMapper;
	
	public CommentDAO commentDtoToCommentDao(CommentDTO commentDto) {
		CommentDAO commentEntity = new CommentDAO();
        modelMapper.map(commentDto, commentEntity);
        return commentEntity;
    }

    public CommentDTO commentDaoToCommentDto(CommentDAO commentDao) {
    	CommentDTO innerComment = new CommentDTO();
    	modelMapper.map(commentDao, innerComment);
        return innerComment;
    }
}
