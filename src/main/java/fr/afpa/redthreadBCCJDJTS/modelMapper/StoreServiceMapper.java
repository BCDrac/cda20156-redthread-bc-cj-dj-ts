package fr.afpa.redthreadBCCJDJTS.modelMapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.redthreadBCCJDJTS.entity.DAO.StoreDAO;
import fr.afpa.redthreadBCCJDJTS.entity.DTO.StoreDTO;

@Service
public class StoreServiceMapper {
	
	@Autowired
	private ModelMapper modelMapper;
	
	public StoreDAO storeDtoToStoreDao(StoreDTO storeDto) {
		StoreDAO storeEntity = new StoreDAO();
		modelMapper.map(storeDto, storeEntity);
        return storeEntity;
    }

    public StoreDTO storeDaoToStoreDto(StoreDAO storeDao) {
    	StoreDTO innerStore = new StoreDTO();
    	modelMapper.map(storeDao, innerStore);
        return innerStore;
    }

}
